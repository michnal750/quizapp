# QuizApp

# 1. Uruchomienie w trybie deweloperskim
1. Pobrać i zainstalować wymagane oprogramowanie:
* SQL Server https://www.microsoft.com/pl-pl/sql-server/sql-server-downloads
* Visual Studio 2019 https://visualstudio.microsoft.com/pl/vs/ wraz z rozszerzeniem do aplikacji asp.net:
![tempsnip.png](./tempsnip.png)
* Visual Studio Code https://code.visualstudio.com/
* Node js https://nodejs.org/en/
2. Pobrać źródła projektu z repozytorium
3. Otworzyć QuizApp/QuizApp.sln
4. W celu utworzenia bazy danych uruchomić Package Manager Console i wpisać komendę update-database
* konsola znajduje się w Tools/NuGet Package Manager/Package Manager Console
* do zarządzania bazą danych można wykorzystać program SQL Server Management Studio
5. Ustawić projekt QuizApp.Api jako projekt startowy
* prawy przycisk na projekcie i ustawienie Set as Startup project
6. Uruchomić część backendową projektu
* wybranie opcji Debug/Start Debugging
* uruchomione web api
![image.png](./image.png)
7. W folderze QuizAppPortal uruchomić Visual Studio Code
8. W terminalu wpisać polecenie npm install
9. W terminalu wpisać polecenie ng serve
* uruchomiona aplikacja
![image-1.png](./image-1.png)

# 2. Instrukcja dla użytkownika

1. Po uruchomieniu aplikacji pokazuje się okno logowania:
![image-2.png](./image-2.png)
2. Jeżeli użytkownik nie ma konta należy je utworzyć za pomocą formularza rejestracji
![image-3.png](./image-3.png)
3. Po zalogowaniu pokazuje się główne okno aplikacji:
![image-4.png](./image-4.png)
Pozwala ono na:
* Take quiz - przeglądanie listy quizów i  rozwiązanie wybranego
* My results - przeglądanie wszystkich wyników użytkownika
* My quizes - przeglądanie quizów użytkownika, a także ich edycję i usuwanie
* Add quiz - dodanie nowego quizu
* My account - przeglądanie i edycja konta użytkownika

W przypadku admina pojawiają się także dwie dodatkowe zakładki:
* Categories - zarządzanie kategoriami
* Users - zarządzanie użytkownikami

# 3. Opis Api

Aplikacja posiada 5 kontrolerów które umożliwiają dostęp do zasobów bazodanowych:\
**1. Auth Controller**
* POST /api/auth/login\
 Umożliwia logowanie, zwraca token
* PATCH /api/auth/password/by-user\
 Zmiana hasła przez usera
* PATCH /api/auth/password/by-admin\
 Zmiana hasła przed admina

**2. Category Controller**
* GET ​/api​/category\
 Pobranie wszystkich kategorii
* POST /api​/category **[ADMIN]**\
 Dodanie nowej kategorii
* PUT /api/category **[ADMIN]**\
 Aktualizacja istniejącej kategorii

 **3. Quiz Controller**
 * GET /api​/quiz\
 Pobranie wszystkich quizów
 * POST /api​/quiz\
 Dodanie nowego quizu
 * PUT ​/api​/quiz\
 Aktualizacja istniejącego quizu
 * GET /api​/quiz​/user\
 Pobranie wszystkich quizów użytkownika
 * GET /api/quiz/{quizId}\
 Pobranie szczegółów quizu
 * DELETE /api​/quiz​/{quizId}\
 Usunięcie quizu
 * POST /api/quiz/check\
 Sprawdzenie quizu i zwrócenie wyniku

 **4. Result Controller**
 * GET ​/api​/result\
 Pobranie wszystkich wyników użytkownika

 **5. User Controller**
 * POST /api​/user\
 Dodanie nowego użytkownika
 * PUT /api​/user\
 Aktualizacja istniejącego użytkownika
 * GET /api​/user **[ADMIN]**\
Pobranie wszystkich użytkowników
* GET /api​/user​/{userId}\
Pobranie szczegółów użytkownika
* DELETE /api​/user​/{userId} **[ADMIN]**\
Usunięcie użytkownika




 

