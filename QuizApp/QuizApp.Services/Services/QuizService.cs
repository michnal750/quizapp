﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Interfaces.Services.Utils;
using QuizApp.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.Services.Services
{
    public class QuizService : IQuizService
    {
        private IQuizRepository quizRepository;
        private IQuestionRepository questionRepository;
        private IResultRepository resultRepository;
        private IMapper mapper;
        private IAuthProvider authProvider;

        public QuizService(
            IQuizRepository quizRepository,
            IMapper mapper,
            IResultRepository resultRepository,
            IQuestionRepository questionRepository,
            IAuthProvider authProvider)
        {
            this.quizRepository = quizRepository;
            this.questionRepository = questionRepository;
            this.mapper = mapper;
            this.resultRepository = resultRepository;
            this.authProvider = authProvider;
        }

        public async Task<IEnumerable<Quiz>> GetAllAsync()
        {
            var dbEntities = await quizRepository.GetAllAsync();
            return mapper.Map<IEnumerable<Quiz>>(dbEntities);
        }

        public async Task<IEnumerable<Quiz>> GetAllUserQuizes(int userId)
        {
            var dbEntities = await quizRepository.GetAllUserQuizesAsync(userId);
            return mapper.Map<IEnumerable<Quiz>>(dbEntities);
        }

        public async Task<QuizDetails> GetDetailsAsync(int quizId)
        {
            var quizEnitity = await quizRepository.GetDetailsAsync(quizId);
            quizEnitity.Questions = await questionRepository.GetAllQuizQuestionsAsync(quizId);
            return mapper.Map<QuizDetails>(quizEnitity);
        }

        public async Task<Result> CheckCompletedQuizAsync(CompletedQuiz completedQuiz)
        {
            var questionEntities = await questionRepository.GetAllQuizQuestionsAsync(completedQuiz.QuizId);

            int numberOfCorrectAnswers = 0;
            foreach (var question in questionEntities)
            {
                var selectedAnswer = completedQuiz.SelectedAnswers.Where(x => x.QuestionId == question.Id).FirstOrDefault();
                if (selectedAnswer.SelectedAnswer == question.CorrectAnswerNumber)
                {
                    numberOfCorrectAnswers++;
                }
            }

            var resultEntity = new ResultEntity
            {
                NumberOfCorrectAnswers = numberOfCorrectAnswers,
                NumberOfAllQuestions = questionEntities.Count(),
                QuizId = completedQuiz.QuizId,
                CreatedById = completedQuiz.UserId,
                CreatedDate = DateTime.Now,
                IsDeleted = false
            };

            resultRepository.Add(resultEntity);
            await resultRepository.SaveChangesAsync();
            resultEntity = await resultRepository.GetByIdAsync(resultEntity.Id);
            return mapper.Map<Result>(resultEntity);
        }

        public async Task<QuizDetails> AddAsync(QuizAdd quizAdd, int createdById)
        {
            var questionEntities = new List<QuestionEntity>();
            foreach (var question in quizAdd.Questions)
            {
                questionEntities.Add(new QuestionEntity
                {
                    QuestionContent = question.QuestionContent,
                    Answer1 = question.Answer1,
                    Answer2 = question.Answer2,
                    Answer3 = question.Answer3,
                    Answer4 = question.Answer4,
                    CorrectAnswerNumber = question.CorrectAnswerNumber,
                    CreatedById = createdById,
                    CreatedDate = DateTime.Now,
                    IsDeleted = false,
                });
            }

            var quizEntity = new QuizEnitity
            {
                Name = quizAdd.Name,
                CategoryId = quizAdd.CategoryId,
                Difficulty = quizAdd.Difficulty,
                TimeLimitInSeconds = quizAdd.TimeLimitInSeconds,
                CreatedById = createdById,
                CreatedDate = DateTime.Now,
                IsDeleted = false,
                Questions = questionEntities,
                NumberOfQuestions = questionEntities.Count
            };

            quizRepository.Add(quizEntity);
            await quizRepository.SaveChangesAsync();
            return await GetDetailsAsync(quizEntity.Id);
        }

        public async Task<QuizDetails> DeleteAsync(int quizId, int deletedById)
        {
            var quizEntity = await quizRepository.GetByIdAsync(quizId);

            if (quizEntity.CreatedById != deletedById && authProvider.GetUserRole() != "Admin")
            {
                throw new Exception();
            }
            quizEntity.IsDeleted = true;
            quizEntity.ModificationDate = DateTime.Now;
            quizEntity.ModifiedById = deletedById;

            var questionsEntities = await questionRepository.GetAllQuizQuestionsAsync(quizId);
            foreach (var questionEntity in questionsEntities)
            {
                questionEntity.IsDeleted = true;
                questionEntity.ModificationDate = DateTime.Now;
                questionEntity.ModifiedById = deletedById;
            }

            quizRepository.Update(quizEntity);
            questionRepository.UpdateRange(questionsEntities);
            await quizRepository.SaveChangesAsync();
            await questionRepository.SaveChangesAsync();
            return await GetDetailsAsync(quizEntity.Id);
        }

        public async Task<QuizDetails> UpdateAsync(QuizModify quizModify, int modifiedById)
        {
            var quizEntity = await quizRepository.GetByIdAsync(quizModify.Id);

            if (quizEntity.CreatedById != modifiedById)
            {
                throw new Exception();
            }

            if (IsQuizChanged(quizEntity, quizModify))
            {
                quizEntity.Name = quizModify.Name;
                quizEntity.CategoryId = quizModify.CategoryId;
                quizEntity.Difficulty = quizModify.Difficulty;
                quizEntity.TimeLimitInSeconds = quizModify.TimeLimitInSeconds;
                quizEntity.NumberOfQuestions = quizModify.Questions.Count();
                quizEntity.ModificationDate = DateTime.Now;
                quizEntity.ModifiedById = modifiedById;
            }

            var questionEntities = await questionRepository.GetAllQuizQuestionsAsync(quizModify.Id);

            var questionEntitiesToAdd = new List<QuestionEntity>();
            var questionEntitiesToUpdate = new List<QuestionEntity>();

            foreach (var questionModify in quizModify.Questions)
            {
                if (questionModify.Id == null)
                {
                    questionEntitiesToAdd.Add(new QuestionEntity
                    {
                        QuestionContent = questionModify.QuestionContent,
                        Answer1 = questionModify.Answer1,
                        Answer2 = questionModify.Answer2,
                        Answer3 = questionModify.Answer3,
                        Answer4 = questionModify.Answer4,
                        CorrectAnswerNumber = questionModify.CorrectAnswerNumber,
                        QuizId = quizEntity.Id,
                        CreatedDate = DateTime.Now,
                        CreatedById = modifiedById,
                        IsDeleted = false
                    });
                }
                else
                {
                    var questionEntity = questionEntities.FirstOrDefault(x => x.Id == questionModify.Id);

                    if (questionEntity != null && IsQuestionChanged(questionEntity, questionModify))
                    {
                        questionEntity.QuestionContent = questionModify.QuestionContent;
                        questionEntity.Answer1 = questionModify.Answer1;
                        questionEntity.Answer2 = questionModify.Answer2;
                        questionEntity.Answer3 = questionModify.Answer3;
                        questionEntity.Answer4 = questionModify.Answer4;
                        questionEntity.CorrectAnswerNumber = questionModify.CorrectAnswerNumber;
                        questionEntity.ModificationDate = DateTime.Now;
                        questionEntity.ModifiedById = modifiedById;

                        questionEntitiesToUpdate.Add(questionEntity);
                    }
                }
            }

            var modifiedQuestionsIds = quizModify.Questions.Select(x => x.Id);
            var questionEntitiesToDelete = questionEntities.Where(x => !modifiedQuestionsIds.Contains(x.Id)).ToList();

            foreach (var questionEntity in questionEntitiesToDelete)
            {
                questionEntity.IsDeleted = true;
                questionEntity.ModificationDate = DateTime.Now;
                questionEntity.ModifiedById = modifiedById;
            }

            quizRepository.Update(quizEntity);
            questionRepository.AddRange(questionEntitiesToAdd);
            questionRepository.UpdateRange(questionEntitiesToUpdate);
            questionRepository.UpdateRange(questionEntitiesToDelete);
            await quizRepository.SaveChangesAsync();
            await questionRepository.SaveChangesAsync();
            return await GetDetailsAsync(quizEntity.Id);
        }

        private bool IsQuizChanged(QuizEnitity quizEntity, QuizModify quizModify)
        {
            return (quizEntity.Name != quizModify.Name ||
                    quizEntity.CategoryId != quizModify.CategoryId ||
                    quizEntity.Difficulty != quizModify.Difficulty ||
                    quizEntity.TimeLimitInSeconds != quizModify.TimeLimitInSeconds ||
                    quizEntity.NumberOfQuestions != quizModify.Questions.Count());
        }

        private bool IsQuestionChanged(QuestionEntity questionEntity, QuestionModify questionModify)
        {
            return (questionEntity.QuestionContent != questionModify.QuestionContent ||
                    questionEntity.Answer1 != questionModify.Answer1 ||
                    questionEntity.Answer2 != questionModify.Answer2 ||
                    questionEntity.Answer3 != questionModify.Answer3 ||
                    questionEntity.Answer4 != questionModify.Answer4 ||
                    questionEntity.CorrectAnswerNumber != questionModify.CorrectAnswerNumber);
        }
    }
}
