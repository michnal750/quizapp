﻿using QuizApp.Services.Interfaces.Services.Utils;
using System.Net;
using System.Net.Mail;

namespace QuizApp.Services.Services.Utils
{
    public class EmailService : IEmailService
    {
        public void SendVerificationLinkEmail(string emailTo, int userId, string activationCode)
        {
            var confirmUrl = "https://quizappapi.azurewebsites.net/api/auth/activate?userId=" + userId.ToString() + "&activationCode=" + activationCode;
            var mailFrom = new MailAddress("michallos1234@gmail.com");
            var mailTo = new MailAddress(emailTo);
            string subject = "Your QuizApp account is successfull created";
            string body = "<br/><br/>We are excited to tell you that your account is" +
              " successfully created. Please click on the below link to activate your account" +
              " <br/><br/><a href='" + confirmUrl + "'>" + confirmUrl + "</a> ";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(mailFrom.Address, "Michalek123")

            };
            using (var message = new MailMessage(mailFrom, mailTo)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            smtp.Send(message);
        }

        public string GetVerificationLink(int userId, string activationCode)
        {
            var confirmUrl = "https://quizappapi.azurewebsites.net/api/auth/activate?userId=" + userId.ToString() + "&activationCode=" + activationCode;
            return confirmUrl;
        }
    }
}
