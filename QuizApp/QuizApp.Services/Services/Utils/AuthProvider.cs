﻿using Microsoft.AspNetCore.Http;
using QuizApp.Services.Interfaces.Services.Utils;
using System;
using System.Linq;
using System.Security.Claims;

namespace QuizApp.Services.Services.Utils
{
    public class AuthProvider : IAuthProvider
    {
        private readonly IHttpContextAccessor context;

        public AuthProvider(IHttpContextAccessor context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public int GetUserId()
        {
            return int.Parse(context.HttpContext.User.Claims
                       .First(i => i.Type == ClaimTypes.NameIdentifier).Value);
        }

        public string GetUserRole()
        {
            return context.HttpContext.User.Claims
                      .First(i => i.Type == ClaimTypes.Role).Value;
        }
    }
}
