﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Models.Category;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace QuizApp.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository categoryRepository;
        private IMapper mapper;

        public CategoryService(
            ICategoryRepository categoryRepository,
            IMapper mapper)
        {
            this.categoryRepository = categoryRepository;
            this.mapper = mapper;

        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            var dbEntities = await categoryRepository.GetAllAsync();
            return mapper.Map<IEnumerable<Category>>(dbEntities);
        }

        public async Task<Category> AddAsync(CategoryAdd categoryAdd, int createdById)
        {
            var newCategoryEntity = new CategoryEntity
            {
                Name = categoryAdd.Name,
                CreatedById = createdById,
                CreatedDate = DateTime.Now,
                IsDeleted = false,
            };
            categoryRepository.Add(newCategoryEntity);
            await categoryRepository.SaveChangesAsync();
            newCategoryEntity = await categoryRepository.GetByIdAsync(newCategoryEntity.Id);
            return mapper.Map<Category>(newCategoryEntity);
        }

        public async Task<Category> UpdateAsync(CategoryModify categoryModify, int modifiedById)
        {
            var categoryEntity = await categoryRepository.GetByIdAsync(categoryModify.Id);
            categoryEntity.Name = categoryModify.Name;
            categoryEntity.ModifiedById = modifiedById;
            categoryEntity.ModificationDate = DateTime.Now;

            categoryRepository.Update(categoryEntity);
            await categoryRepository.SaveChangesAsync();

            categoryEntity = await categoryRepository.GetByIdAsync(categoryEntity.Id);

            return mapper.Map<Category>(categoryEntity);
        }
    }
}
