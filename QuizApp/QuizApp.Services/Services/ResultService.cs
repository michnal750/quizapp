﻿using AutoMapper;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.Services.Services
{
    public class ResultService : IResultService
    {
        private readonly IResultRepository resultRepository;
        private readonly IMapper mapper;

        public ResultService(IResultRepository resultRepository, IMapper mapper)
        {
            this.resultRepository = resultRepository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<Result>> GetAllUserResultsAsync(int userId)
        {
            var dbEnitities = await resultRepository.GetAllUserResultsAsync(userId);
            return mapper.Map<ICollection<Result>>(dbEnitities);
        }
    }
}
