﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Interfaces.Services.Utils;
using QuizApp.Services.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.Services.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;
        private readonly IHashService hashService;
        private readonly IEmailService emailService;

        public UserService(
            IUserRepository userRepository,
            IMapper mapper,
            IHashService hashService,
            IEmailService emailService)
        {
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.hashService = hashService;
            this.emailService = emailService;
        }

        public async Task<User> AddUserAsync(UserAdd userAdd)
        {
            var userEntity = new UserEntity
            {
                Username = userAdd.Username,
                Password = hashService.HashPassword(userAdd.Password),
                Email = userAdd.Email,
                FirstName = userAdd.FirstName,
                LastName = userAdd.LastName,
                Role = userAdd.Role.ToString(),
                CreatedDate = DateTime.Now,
                IsDeleted = false,
                IsActivated = false,
                ActivationCode = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8)
        };
            userRepository.Add(userEntity);
            await userRepository.SaveChangesAsync();
            userEntity = await userRepository.GetByIdAsync(userEntity.Id);

            var user = mapper.Map<User>(userEntity);
            user.ActivationLink = emailService.GetVerificationLink(userEntity.Id, userEntity.ActivationCode);
            return user;
        }

        public async Task<User> UpdateUserAsync(UserModify userModify)
        {
            var userEntity = await userRepository.GetByIdAsync(userModify.Id);

            userEntity.FirstName = userModify.FirstName;
            userEntity.LastName = userModify.LastName;
            userEntity.Email = userModify.Email;
            userEntity.ModificationDate = DateTime.Now;

            userRepository.Update(userEntity);
            await userRepository.SaveChangesAsync();
            userEntity = await userRepository.GetByIdAsync(userEntity.Id);
            return mapper.Map<User>(userEntity);
        }

        public async Task<User> GetUserAsync(int userId)
        {
            var userEntity = await userRepository.GetByIdAsync(userId);
            return mapper.Map<User>(userEntity);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            var userEntities = await userRepository.GetAllAsync();
            return mapper.Map<IEnumerable<User>>(userEntities);
        }

        public async Task<User> DeleteAsync(int userId, int deletedById)
        {
            var userEntity = await userRepository.GetByIdAsync(userId);

            userEntity.IsDeleted = true;
            userEntity.ModificationDate = DateTime.Now;

            userRepository.Update(userEntity);
            await userRepository.SaveChangesAsync();
            return await GetUserAsync(userEntity.Id);
        }
    }
}
