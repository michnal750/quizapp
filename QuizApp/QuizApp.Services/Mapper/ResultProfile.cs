﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.Services.Models;

namespace QuizApp.Services.Mapper
{
    public class ResultProfile : Profile
    {
        public ResultProfile()
        {
            CreateMap<ResultEntity, Result>()
                 .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.CreatedById))
                 .ForMember(dest => dest.QuizName, opt => opt.MapFrom(src => src.Quiz.Name));
        }
    }
}
