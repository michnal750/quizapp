﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.Services.Models;

namespace QuizApp.Services.Mapper
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile()
        {
            CreateMap<QuestionEntity, Question>();
        }
    }
}
