﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.Services.Models;

namespace QuizApp.Services.Mapper
{
    public class QuizProfile : Profile
    {
        public QuizProfile()
        {
            CreateMap<QuizEnitity, Quiz>()
                 .ForMember(dest => dest.CreatedByData, opt => opt.MapFrom(src => src.CreatedBy.FirstName + " " + src.CreatedBy.LastName))
                 .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name));

            CreateMap<QuizEnitity, QuizDetails>()
                 .ForMember(dest => dest.CreatedByData, opt => opt.MapFrom(src => src.CreatedBy.FirstName + " " + src.CreatedBy.LastName))
                 .ForMember(dest => dest.ModifiedByData, opt => opt.MapFrom(src => src.ModifiedBy.FirstName + " " + src.ModifiedBy.LastName))
                 .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name));
        }
    }
}
