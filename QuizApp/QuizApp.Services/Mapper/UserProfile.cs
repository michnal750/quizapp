﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.Services.Models;

namespace QuizApp.Services.Mapper
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, User>();
        }
    }
}
