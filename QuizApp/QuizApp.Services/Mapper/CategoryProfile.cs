﻿using AutoMapper;
using QuizApp.DataAccess.Entities;
using QuizApp.Services.Models.Category;

namespace QuizApp.Services.Mapper
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<CategoryEntity, Category>()
                .ForMember(dest => dest.CreatedByData, opt => opt.MapFrom(src => src.CreatedBy.FirstName + " " + src.CreatedBy.LastName));
        }
    }
}
