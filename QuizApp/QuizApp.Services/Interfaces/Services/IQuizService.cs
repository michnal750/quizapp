﻿using QuizApp.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.Services.Interfaces.Services
{
    public interface IQuizService
    {
        Task<IEnumerable<Quiz>> GetAllAsync();
        Task<IEnumerable<Quiz>> GetAllUserQuizes(int userId);
        Task<QuizDetails> GetDetailsAsync(int quizId);
        Task<QuizDetails> AddAsync(QuizAdd quizAdd, int createdById);
        Task<QuizDetails> UpdateAsync(QuizModify quizModify, int modifiedById);
        Task<QuizDetails> DeleteAsync(int quizId, int deletedById);
        Task<Result> CheckCompletedQuizAsync(CompletedQuiz completedQuiz);
    }
}
