﻿using QuizApp.Services.Models.Category;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.Services.Interfaces.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<Category>> GetAllAsync();
        Task<Category> AddAsync(CategoryAdd categoryAdd, int createdById);
        Task<Category> UpdateAsync(CategoryModify categoryModify, int modifiedById);
    }
}
