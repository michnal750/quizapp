﻿namespace QuizApp.Services.Interfaces.Services.Utils
{
    public interface IAuthProvider
    {
        int GetUserId();
        string GetUserRole();
    }
}
