﻿namespace QuizApp.Services.Interfaces.Services.Utils
{
    public interface IEmailService
    {
        void SendVerificationLinkEmail(string emailId, int userId, string activationcode);
        string GetVerificationLink(int userId, string activationcode);
    }
}
