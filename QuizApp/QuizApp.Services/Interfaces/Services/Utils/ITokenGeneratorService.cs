﻿namespace QuizApp.Services.Interfaces.Services.Utils
{
    public interface ITokenGeneratorService
    {
        string GenerateToken(int userId, string userRole);
    }
}
