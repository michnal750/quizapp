﻿using QuizApp.Services.Models;
using System.Threading.Tasks;

namespace QuizApp.Services.Interfaces.Services.Utils
{
    public interface IAuthService
    {
        Task<AuthData> LoginAsync(UserLogin userLogin);
        Task<bool> ActivateUserAsync(int userId, string activationCode);
        Task<bool> ChangePasswordByUserAsync(UserPasswordChange passwordChange);
        Task<bool> ChangePasswordByAdminAsync(AdminPasswordChange passwordChange);
    }
}
