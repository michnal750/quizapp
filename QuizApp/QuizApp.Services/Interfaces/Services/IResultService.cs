﻿using QuizApp.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.Services.Interfaces.Services
{
    public interface IResultService
    {
        Task<IEnumerable<Result>> GetAllUserResultsAsync(int userId);
    }
}
