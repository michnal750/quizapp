﻿using QuizApp.Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.Services.Interfaces.Services
{
    public interface IUserService
    {
        Task<User> AddUserAsync(UserAdd userAdd);
        Task<User> UpdateUserAsync(UserModify userModify);
        Task<User> GetUserAsync(int userId);
        Task<IEnumerable<User>> GetAllAsync();
        Task<User> DeleteAsync(int userId, int deletedById);
    }
}
