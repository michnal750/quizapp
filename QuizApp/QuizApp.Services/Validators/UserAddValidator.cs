﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class UserAddValidator : AbstractValidator<UserAdd>
    {
        public UserAddValidator(IUserRepository userRepository)
        {
            RuleFor(p => p.Username)
                .NotEmpty()
                .MaximumLength(20)
                .MustAsync((x, cancellation) => userRepository.IsUsernameUniqueAsync(x))
                .WithMessage("Username is not unique");

            RuleFor(p => p.Password)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.FirstName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.LastName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.Email)
                .NotEmpty()
                .MaximumLength(50)
                .MustAsync((x, cancellation) => userRepository.IsEmailUniqueAsync(x))
                .WithMessage("Email is not unique");
        }
    }
}
