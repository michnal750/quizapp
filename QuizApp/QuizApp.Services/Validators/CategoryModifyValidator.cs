﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models.Category;

namespace QuizApp.Services.Validators
{
    public class CategoryModifyValidator : AbstractValidator<CategoryModify>
    {
        public CategoryModifyValidator(ICategoryRepository categoryRepository)
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .MustAsync((x, cancellation) => categoryRepository.IsExistAsync(x))
                .WithMessage("Category is not exist");

            RuleFor(p => p.Name)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
