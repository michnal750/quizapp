﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class AdminPasswordChangeValidator : AbstractValidator<AdminPasswordChange>
    {
        public AdminPasswordChangeValidator(IUserRepository userRepository)
        {
            RuleFor(p => p.UserId)
                .NotEmpty()
                .MustAsync((x, cancellation) => userRepository.IsExistAsync(x))
                .WithMessage("User is not exist");

            RuleFor(p => p.NewPassword)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
