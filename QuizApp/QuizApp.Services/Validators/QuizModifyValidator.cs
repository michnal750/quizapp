﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class QuizModifyValidator : AbstractValidator<QuizModify>
    {
        public QuizModifyValidator(IQuizRepository quizRepository, ICategoryRepository categoryRepository)
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .MustAsync((x, cancellation) => quizRepository.IsExistAsync(x))
                .WithMessage("Quiz is not exist");

            RuleFor(p => p.Name)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.CategoryId)
                .NotEmpty()
                .MustAsync((x, cancellation) => categoryRepository.IsExistAsync(x))
                .WithMessage("Category is not exist");

            RuleFor(p => p.Difficulty)
                .NotEmpty()
                .GreaterThan(-1);

            RuleFor(p => p.TimeLimitInSeconds)
                .NotEmpty()
                .GreaterThan(0);

            RuleFor(p => p.Questions)
                .NotEmpty();
        }
    }
}
