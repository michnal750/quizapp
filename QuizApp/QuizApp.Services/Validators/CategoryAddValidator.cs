﻿using FluentValidation;
using QuizApp.Services.Models.Category;

namespace QuizApp.Services.Validators
{
    public class CategoryAddValidator : AbstractValidator<CategoryAdd>
    {
        public CategoryAddValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
