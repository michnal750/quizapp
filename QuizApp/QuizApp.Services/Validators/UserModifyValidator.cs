﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class UserModifyValidator : AbstractValidator<UserModify>
    {
        public UserModifyValidator(IUserRepository userRepository)
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .MustAsync((x, cancellation) => userRepository.IsExistAsync(x))
                .WithMessage("User is not exist");

            RuleFor(p => p.FirstName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.LastName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.Email)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
