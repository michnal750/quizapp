﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class CompletedQuizAnswerValidator : AbstractValidator<CompletedQuizAnswer>
    {
        public CompletedQuizAnswerValidator(IQuestionRepository questionRepository)
        {
            RuleFor(p => p.QuestionId)
                 .NotEmpty()
                 .MustAsync((x, cancellation) => questionRepository.IsExistAsync(x))
                 .WithMessage("Question is not exist");
        }
    }
}
