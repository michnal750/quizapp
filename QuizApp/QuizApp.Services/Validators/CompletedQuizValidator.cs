﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class CompletedQuizValidator : AbstractValidator<CompletedQuiz>
    {
        public CompletedQuizValidator(IQuizRepository quizRepository, IUserRepository userRepository)
        {
            RuleFor(p => p.QuizId)
                 .NotEmpty()
                 .MustAsync((x, cancellation) => quizRepository.IsExistAsync(x))
                 .WithMessage("Quiz is not exist");

            RuleFor(p => p.UserId)
                 .NotEmpty()
                 .MustAsync((x, cancellation) => userRepository.IsExistAsync(x))
                 .WithMessage("User is not exist");

            RuleFor(p => p.SelectedAnswers)
                 .NotEmpty();
        }
    }
}
