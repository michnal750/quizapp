﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class QuestionModifyValidator : AbstractValidator<QuestionModify>
    {
        public QuestionModifyValidator(IQuestionRepository questionRepository)
        {
            RuleFor(p => p.Id)
                .MustAsync((x, cancellation) => questionRepository.IsExistAsync(x.Value))
                .When(p => p.Id.HasValue)
                .WithMessage("Quiz is not exist");

            RuleFor(p => p.QuestionContent)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer1)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer2)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer3)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer4)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.CorrectAnswerNumber)
                .NotEmpty()
                .GreaterThan(0)
                .LessThan(5);
        }
    }
}
