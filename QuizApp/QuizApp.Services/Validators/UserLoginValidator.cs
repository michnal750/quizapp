﻿using FluentValidation;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class UserLoginValidator : AbstractValidator<UserLogin>
    {
        public UserLoginValidator()
        {
            RuleFor(p => p.Username)
                .NotEmpty()
                .MaximumLength(20);

            RuleFor(p => p.Password)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
