﻿using FluentValidation;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class UserPasswordChangeValidator : AbstractValidator<UserPasswordChange>
    {
        public UserPasswordChangeValidator()
        {
            RuleFor(p => p.CurrentPassword)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.NewPassword)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
