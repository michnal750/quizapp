﻿using FluentValidation;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class QuizAddValidator : AbstractValidator<QuizAdd>
    {
        public QuizAddValidator(ICategoryRepository categoryRepository)
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(p => p.CategoryId)
                .NotEmpty()
                .MustAsync((x, cancellation) => categoryRepository.IsExistAsync(x))
                .WithMessage("Category is not exist");

            RuleFor(p => p.Difficulty)
                .NotEmpty()
                .GreaterThan(-1);

            RuleFor(p => p.TimeLimitInSeconds)
                .NotEmpty()
                .GreaterThan(0);

            RuleFor(p => p.Questions)
                .NotEmpty();
        }
    }
}
