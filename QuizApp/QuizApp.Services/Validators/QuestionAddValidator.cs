﻿using FluentValidation;
using QuizApp.Services.Models;

namespace QuizApp.Services.Validators
{
    public class QuestionAddValidator : AbstractValidator<QuestionAdd>
    {
        public QuestionAddValidator()
        {
            RuleFor(p => p.QuestionContent)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer1)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer2)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer3)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.Answer4)
                .NotEmpty()
                .MaximumLength(200);

            RuleFor(p => p.CorrectAnswerNumber)
                .NotEmpty()
                .GreaterThan(0)
                .LessThan(5);
        }
    }
}
