﻿using System;

namespace QuizApp.Services.Models
{
    public class Result
    {
        public int Id { get; set; }

        public int QuizId { get; set; }

        public string QuizName { get; set; }

        public int UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public int NumberOfCorrectAnswers { get; set; }

        public int NumberOfAllQuestions { get; set; }
    }
}
