﻿namespace QuizApp.Services.Models
{
    public class UserPasswordChange
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
