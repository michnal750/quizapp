﻿namespace QuizApp.Services.Models
{
    public class AdminPasswordChange
    {
        public int UserId { get; set; }
        public string NewPassword { get; set; }
    }
}
