﻿using QuizApp.Services.Enums;
using System;

namespace QuizApp.Services.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserRole Role { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ActivationLink { get; set; }
    }
}
