﻿using QuizApp.Services.Enums;

namespace QuizApp.Services.Models
{
    public class UserAdd
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public UserRole Role { get; set; }
    }
}
