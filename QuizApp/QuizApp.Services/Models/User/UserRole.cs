﻿namespace QuizApp.Services.Enums
{
    public enum UserRole
    {
        User,
        Admin
    }
}
