﻿namespace QuizApp.Services.Models
{
    public class AuthData
    {
        public string Token { get; set; }
        public User User { get; set; }
    }
}
