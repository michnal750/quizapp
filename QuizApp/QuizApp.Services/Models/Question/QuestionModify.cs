﻿namespace QuizApp.Services.Models
{
    public class QuestionModify
    {
        public int? Id { get; set; }
        public string QuestionContent { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public int CorrectAnswerNumber { get; set; }
    }
}
