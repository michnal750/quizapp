﻿using System.Collections.Generic;

namespace QuizApp.Services.Models
{
    public class CompletedQuiz
    {
        public int QuizId { get; set; }
        public int UserId { get; set; }
        public ICollection<CompletedQuizAnswer> SelectedAnswers { get; set; }
    }
}
