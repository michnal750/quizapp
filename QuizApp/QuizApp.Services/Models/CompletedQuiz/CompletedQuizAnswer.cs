﻿namespace QuizApp.Services.Models
{
    public class CompletedQuizAnswer
    {
        public int QuestionId { get; set; }
        public int? SelectedAnswer { get; set; }
    }
}
