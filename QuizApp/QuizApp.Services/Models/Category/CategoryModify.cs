﻿namespace QuizApp.Services.Models.Category
{
    public class CategoryModify
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
