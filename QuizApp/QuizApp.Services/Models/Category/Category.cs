﻿using System;

namespace QuizApp.Services.Models.Category
{
    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedById { get; set; }

        public string CreatedByData { get; set; }
    }
}
