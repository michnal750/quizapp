﻿using System.Collections.Generic;

namespace QuizApp.Services.Models
{
    public class QuizModify
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int TimeLimitInSeconds { get; set; }
        public int Difficulty { get; set; }
        public IEnumerable<QuestionModify> Questions { get; set; }
    }
}
