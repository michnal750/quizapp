﻿using System;
using System.Collections.Generic;

namespace QuizApp.Services.Models
{
    public class QuizDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public int Difficulty { get; set; }

        public int TimeLimitInSeconds { get; set; }

        public int NumberOfQuestions { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedById { get; set; }

        public string CreatedByData { get; set; }

        public DateTime? ModificationDate { get; set; }

        public int? ModifiedById { get; set; }

        public string ModifiedByData { get; set; }

        public ICollection<Question> Questions { get; set; }
    }
}
