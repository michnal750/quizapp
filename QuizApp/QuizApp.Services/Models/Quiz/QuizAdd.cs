﻿using System.Collections.Generic;

namespace QuizApp.Services.Models
{
    public class QuizAdd
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int TimeLimitInSeconds { get; set; }
        public int Difficulty { get; set; }
        public ICollection<QuestionAdd> Questions { get; set; }
    }
}