﻿using System;

namespace QuizApp.Services.Models
{
    public class Quiz
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public int Difficulty { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedById { get; set; }

        public string CreatedByData { get; set; }
    }
}
