﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Interfaces.Services.Utils;
using System.Threading.Tasks;

namespace QuizApp.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/result")]
    public class ResultController : ControllerBase
    {
        private readonly IResultService resultService;
        private readonly IAuthProvider authProvider;

        public ResultController(IResultService resultService, IAuthProvider authProvider)
        {
            this.resultService = resultService;
            this.authProvider = authProvider;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllUserResults()
        {
            var userId = authProvider.GetUserId();
            var result = await resultService.GetAllUserResultsAsync(userId);
            return Ok(result);
        }
    }
}
