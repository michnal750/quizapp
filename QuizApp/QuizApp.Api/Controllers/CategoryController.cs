﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Interfaces.Services.Utils;
using QuizApp.Services.Models.Category;
using System.Threading.Tasks;

namespace QuizApp.Api.Controllers
{
    [ApiController]
    [Route("api/category")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;
        private readonly IAuthProvider authProvider;
        public CategoryController(
            ICategoryService categoryService,
            IAuthProvider authProvider)
        {
            this.categoryService = categoryService;
            this.authProvider = authProvider;
        }

        [Authorize]
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAll()
        {
            var result = await categoryService.GetAllAsync();
            return Ok(result);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Add([FromBody] CategoryAdd categoryAdd)
        {
            var userId = authProvider.GetUserId();
            var result = await categoryService.AddAsync(categoryAdd, userId);
            return Ok(result);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        [Route("")]
        public async Task<IActionResult> Update([FromBody] CategoryModify categoryModify)
        {
            var userId = authProvider.GetUserId();
            var result = await categoryService.UpdateAsync(categoryModify, userId);
            return Ok(result);
        }
    }
}
