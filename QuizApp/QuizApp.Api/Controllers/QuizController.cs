﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Interfaces.Services.Utils;
using QuizApp.Services.Models;
using System.Threading.Tasks;

namespace QuizApp.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/quiz")]
    public class QuizController : ControllerBase
    {
        private readonly IQuizService quizService;
        private readonly IAuthProvider authProvider;

        public QuizController(IQuizService quizService, IAuthProvider authProvider)
        {
            this.quizService = quizService;
            this.authProvider = authProvider;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAll()
        {
            var result = await quizService.GetAllAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("user")]
        public async Task<IActionResult> GetUserQuizes()
        {
            var userId = authProvider.GetUserId();
            var result = await quizService.GetAllUserQuizes(userId);
            return Ok(result);
        }

        [HttpGet]
        [Route("{quizId}")]
        public async Task<IActionResult> GetDetails(int quizId)
        {
            var result = await quizService.GetDetailsAsync(quizId);
            return Ok(result);
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Add([FromBody] QuizAdd quizAdd)
        {
            var userId = authProvider.GetUserId();
            var result = await quizService.AddAsync(quizAdd, userId);
            return Ok(result);
        }

        [HttpPut]
        [Route("")]
        public async Task<IActionResult> Update([FromBody] QuizModify quizModify)
        {
            var userId = authProvider.GetUserId();
            var result = await quizService.UpdateAsync(quizModify, userId);
            return Ok(result);
        }

        [HttpDelete]
        [Route("{quizId}")]
        public async Task<IActionResult> Delete(int quizId)
        {
            var userId = authProvider.GetUserId();
            var result = await quizService.DeleteAsync(quizId, userId);
            return Ok(result);
        }

        [HttpPost]
        [Route("check")]
        public async Task<IActionResult> CheckCompletedQuiz([FromBody] CompletedQuiz completedQuiz)
        {
            var result = await quizService.CheckCompletedQuizAsync(completedQuiz);
            return Ok(result);
        }
    }
}
