
using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using QuizApp.DataAccess;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.DataAccess.Repositories;
using QuizApp.Services.Interfaces.Services;
using QuizApp.Services.Interfaces.Services.Utils;
using QuizApp.Services.Mapper;
using QuizApp.Services.Models;
using QuizApp.Services.Services;
using QuizApp.Services.Services.Utils;
using QuizApp.Services.Validators;

namespace QuizApp.WebApi
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDataAccessServices(this IServiceCollection services, string connectionString)
        {
            services.AddDbContextPool<QuizContext>(options =>
              options.UseSqlServer(connectionString));
            return services;
        }

        public static IServiceCollection AddMappingServices(this IServiceCollection services)
        {
            services.AddSingleton<Profile, UserProfile>();
            services.AddSingleton<Profile, QuizProfile>();
            services.AddSingleton<Profile, QuestionProfile>();
            services.AddSingleton<Profile, ResultProfile>();
            services.AddSingleton<Profile, CategoryProfile>();
            services.AddSingleton<IConfigurationProvider, AutoMapperConfiguration>(p =>
                    new AutoMapperConfiguration(p.GetServices<Profile>()));
            services.AddSingleton<IMapper, Mapper>();
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddScoped<IHashService, HashService>();
            services.AddScoped<ITokenGeneratorService, TokenGeneratorService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IAuthProvider, AuthProvider>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IQuizService, QuizService>();
            services.AddScoped<IResultService, ResultService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IEmailService, EmailService>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IQuizRepository, QuizRepository>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IResultRepository, ResultRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            return services;
        }

        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddScoped<IValidator<UserLogin>, UserLoginValidator>();
            services.AddScoped<IValidator<UserAdd>, UserAddValidator>();
            services.AddScoped<IValidator<UserModify>, UserModifyValidator>();
            services.AddScoped<IValidator<UserPasswordChange>, UserPasswordChangeValidator>();
            services.AddScoped<IValidator<AdminPasswordChange>, AdminPasswordChangeValidator>();
            services.AddScoped<IValidator<CompletedQuiz>, CompletedQuizValidator>();
            services.AddScoped<IValidator<CompletedQuizAnswer>, CompletedQuizAnswerValidator>();
            services.AddScoped<IValidator<QuizAdd>, QuizAddValidator>();
            services.AddScoped<IValidator<QuizModify>, QuizModifyValidator>();
            services.AddScoped<IValidator<QuestionAdd>, QuestionAddValidator>();
            services.AddScoped<IValidator<QuestionModify>, QuestionModifyValidator>();
            return services;
        }
    }
}
