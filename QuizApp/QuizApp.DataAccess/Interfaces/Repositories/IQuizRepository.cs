﻿using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Interfaces.Repositories
{
    public interface IQuizRepository : IRepositoryBase<QuizEnitity>
    {
        new Task<IEnumerable<QuizEnitity>> GetAllAsync();
        Task<IEnumerable<QuizEnitity>> GetAllUserQuizesAsync(int userId);
        Task<QuizEnitity> GetDetailsAsync(int quizId);
    }
}
