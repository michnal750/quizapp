﻿using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Interfaces.Repositories
{
    public interface IUserRepository : IRepositoryBase<UserEntity>
    {
        Task<UserEntity> GetByUsernameAsync(string username);
        Task<bool> IsUsernameUniqueAsync(string username);
        Task<bool> IsEmailUniqueAsync(string email);
        new Task<IEnumerable<UserEntity>> GetAllAsync();
    }
}
