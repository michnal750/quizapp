﻿using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Interfaces.Repositories
{
    public interface IResultRepository : IRepositoryBase<ResultEntity>
    {
        Task<IEnumerable<ResultEntity>> GetAllUserResultsAsync(int userId);
    }
}
