﻿using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Interfaces.Repositories
{
    public interface ICategoryRepository : IRepositoryBase<CategoryEntity>
    {
        new Task<IEnumerable<CategoryEntity>> GetAllAsync();
    }
}
