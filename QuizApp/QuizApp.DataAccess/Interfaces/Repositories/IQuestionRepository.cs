﻿using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Interfaces.Repositories
{
    public interface IQuestionRepository : IRepositoryBase<QuestionEntity>
    {
        Task<IEnumerable<QuestionEntity>> GetAllQuizQuestionsAsync(int quizId);
        void AddRange(IEnumerable<QuestionEntity> entities);
        void UpdateRange(IEnumerable<QuestionEntity> entities);
    }
}
