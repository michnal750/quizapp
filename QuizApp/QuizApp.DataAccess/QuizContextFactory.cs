﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace QuizApp.DataAccess
{
    public class QuizContextFactory : IDesignTimeDbContextFactory<QuizContext>
    {
        public QuizContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<QuizContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Initial Catalog=QuizDb;MultipleActiveResultSets=true;Integrated Security=true");
            return new QuizContext(optionsBuilder.Options);
        }
    }
}
