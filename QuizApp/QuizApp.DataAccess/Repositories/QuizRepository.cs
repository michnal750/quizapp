﻿using Microsoft.EntityFrameworkCore;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.DataAccess.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Repositories
{
    public class QuizRepository : RepositoryBase<QuizEnitity>, IQuizRepository
    {
        public QuizRepository(QuizContext quizContext) : base(quizContext)
        {

        }

        public new async Task<IEnumerable<QuizEnitity>> GetAllAsync()
        {
            return await QuizContext.Quizes
                .Where(x => !x.IsDeleted)
                .Include(x => x.CreatedBy)
                .Include(x => x.Category)
                .ToListAsync();
        }

        public async Task<IEnumerable<QuizEnitity>> GetAllUserQuizesAsync(int userId)
        {
            return await QuizContext.Quizes
                .Where(x => !x.IsDeleted && x.CreatedById == userId)
                .Include(x => x.CreatedBy)
                .Include(x => x.Category)
                .ToListAsync();
        }

        public async Task<QuizEnitity> GetDetailsAsync(int quizId)
        {
            return await QuizContext.Quizes
                .Include(x => x.CreatedBy)
                .Include(x => x.ModifiedBy)
                .Include(x => x.Category)
                .FirstOrDefaultAsync(x => x.Id == quizId);
        }
    }
}
