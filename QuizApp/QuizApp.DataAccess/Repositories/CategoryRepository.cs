﻿using Microsoft.EntityFrameworkCore;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.DataAccess.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Repositories
{
    public class CategoryRepository : RepositoryBase<CategoryEntity>, ICategoryRepository
    {
        public CategoryRepository(QuizContext quizContext) : base(quizContext)
        {

        }

        public new async Task<IEnumerable<CategoryEntity>> GetAllAsync()
        {
            return await QuizContext.Categories
                .Where(x => !x.IsDeleted)
                .Include(x => x.CreatedBy)
                .ToListAsync();
        }
    }
}
