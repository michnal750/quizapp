﻿using Microsoft.EntityFrameworkCore;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.DataAccess.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Repositories
{
    public class ResultRepository : RepositoryBase<ResultEntity>, IResultRepository
    {
        public ResultRepository(QuizContext quizContext) : base(quizContext)
        {

        }

        public async Task<IEnumerable<ResultEntity>> GetAllUserResultsAsync(int userId)
        {
            return await QuizContext.Results.Where(x => x.CreatedById == userId && !x.IsDeleted)
                .Include(x => x.Quiz).ToListAsync();
        }
    }
}
