﻿using Microsoft.EntityFrameworkCore;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.DataAccess.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Repositories
{
    public class UserRepository : RepositoryBase<UserEntity>, IUserRepository
    {
        public UserRepository(QuizContext quizContext) : base(quizContext)
        {

        }

        public async Task<UserEntity> GetByUsernameAsync(string username)
        {
            return await QuizContext.Set<UserEntity>().FirstOrDefaultAsync(x => x.Username == username);
        }

        public async Task<bool> IsUsernameUniqueAsync(string username)
        {
            return !(await QuizContext.Set<UserEntity>().AnyAsync(x => x.Username == username && !x.IsDeleted));
        }

        public async Task<bool> IsEmailUniqueAsync(string email)
        {
            return !(await QuizContext.Set<UserEntity>().AnyAsync(x => x.Email == email && !x.IsDeleted));
        }

        public async new Task<IEnumerable<UserEntity>> GetAllAsync()
        {
            return await QuizContext.Users.Where(x => !x.IsDeleted).ToListAsync();
        }
    }
}
