﻿using Microsoft.EntityFrameworkCore;
using QuizApp.DataAccess.Entities;
using QuizApp.DataAccess.Interfaces.Repositories;
using QuizApp.DataAccess.Repositories.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApp.DataAccess.Repositories
{
    public class QuestionRepository : RepositoryBase<QuestionEntity>, IQuestionRepository
    {
        public QuestionRepository(QuizContext quizContext) : base(quizContext)
        {

        }

        public async Task<IEnumerable<QuestionEntity>> GetAllQuizQuestionsAsync(int quizId)
        {
            return await QuizContext.Questions
                .Where(x => !x.IsDeleted && x.QuizId == quizId)
                .ToListAsync();
        }

        public void AddRange(IEnumerable<QuestionEntity> entities)
        {
            QuizContext.Questions.AddRangeAsync(entities);
        }

        public void UpdateRange(IEnumerable<QuestionEntity> entities)
        {
            QuizContext.Questions.UpdateRange(entities);
        }
    }
}
