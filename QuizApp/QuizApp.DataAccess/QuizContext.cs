﻿using Microsoft.EntityFrameworkCore;
using QuizApp.DataAccess.Configurations;
using QuizApp.DataAccess.Entities;

namespace QuizApp.DataAccess
{
    public class QuizContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<QuizEnitity> Quizes { get; set; }
        public DbSet<QuestionEntity> Questions { get; set; }
        public DbSet<ResultEntity> Results { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }

        public QuizContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<UserEntity>(new UserConfiguration());
            modelBuilder.ApplyConfiguration<QuizEnitity>(new QuizConfiguration());
            modelBuilder.ApplyConfiguration<QuestionEntity>(new QuestionConfiguration());
            modelBuilder.ApplyConfiguration<ResultEntity>(new ResultConfiguration());
            modelBuilder.ApplyConfiguration<CategoryEntity>(new CategoryConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
