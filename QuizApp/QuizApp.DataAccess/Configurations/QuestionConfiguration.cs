﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApp.DataAccess.Entities;

namespace QuizApp.DataAccess.Configurations
{
    public class QuestionConfiguration : IEntityTypeConfiguration<QuestionEntity>
    {
        public void Configure(EntityTypeBuilder<QuestionEntity> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.QuestionContent)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.Answer1)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.Answer2)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.Answer3)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.Answer4)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.CreatedDate)
                .IsRequired();

            builder.Property(x => x.ModificationDate)
                .IsRequired(false);

            builder.Property(x => x.IsDeleted)
                .IsRequired();

            builder.HasOne(x => x.CreatedBy)
                .WithMany(y => y.CreatedQuestions)
                .HasForeignKey(x => x.CreatedById)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.ModifiedBy)
                .WithMany(x => x.ModifiedQuestions)
                .HasForeignKey(x => x.ModifiedById)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Quiz)
                .WithMany(y => y.Questions)
                .HasForeignKey(x => x.QuizId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
