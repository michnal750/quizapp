﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApp.DataAccess.Entities;

namespace QuizApp.DataAccess.Configurations
{
    public class ResultConfiguration : IEntityTypeConfiguration<ResultEntity>
    {
        public void Configure(EntityTypeBuilder<ResultEntity> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.NumberOfCorrectAnswers)
                .IsRequired();

            builder.Property(x => x.NumberOfAllQuestions)
                .IsRequired();

            builder.Property(x => x.CreatedDate)
                .IsRequired();

            builder.Property(x => x.IsDeleted)
                .IsRequired();

            builder.HasOne(x => x.CreatedBy)
                .WithMany(y => y.CreatedResults)
                .HasForeignKey(x => x.CreatedById)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Quiz)
                .WithMany(y => y.Results)
                .HasForeignKey(x => x.QuizId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
