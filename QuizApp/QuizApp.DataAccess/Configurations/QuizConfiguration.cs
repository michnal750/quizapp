﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApp.DataAccess.Entities;

namespace QuizApp.DataAccess.Configurations
{
    public class QuizConfiguration : IEntityTypeConfiguration<QuizEnitity>
    {
        public void Configure(EntityTypeBuilder<QuizEnitity> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasMaxLength(50)
                .IsRequired();


            builder.Property(x => x.Difficulty)
                .IsRequired();

            builder.Property(x => x.NumberOfQuestions)
                .IsRequired();

            builder.Property(x => x.TimeLimitInSeconds)
                .IsRequired();

            builder.Property(x => x.CreatedDate)
                .IsRequired();

            builder.Property(x => x.ModificationDate)
                .IsRequired(false);

            builder.Property(x => x.IsDeleted)
                .IsRequired();

            builder.HasOne(x => x.CreatedBy)
                .WithMany(y => y.CreatedQuizes)
                .HasForeignKey(x => x.CreatedById)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.ModifiedBy)
                .WithMany(x => x.ModifiedQuizes)
                .HasForeignKey(x => x.ModifiedById)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(x => x.Category)
                .WithMany(y => y.Quizzes)
                .HasForeignKey(x => x.CategoryId)
                .IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
