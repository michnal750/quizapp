﻿namespace QuizApp.DataAccess.Entities
{
    public class ResultEntity : BaseEntity
    {
        public int NumberOfCorrectAnswers { get; set; }
        public int NumberOfAllQuestions { get; set; }

        public int QuizId { get; set; }
        public virtual QuizEnitity Quiz { get; set; }
    }
}
