﻿using System.Collections.Generic;

namespace QuizApp.DataAccess.Entities
{
    public class CategoryEntity : ModifiableEntity
    {
        public string Name { get; set; }

        public virtual IEnumerable<QuizEnitity> Quizzes { get; set; }
    }
}
