﻿using System;

namespace QuizApp.DataAccess.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedById { get; set; }

        public virtual UserEntity CreatedBy { get; set; }

        public bool IsDeleted { get; set; }
    }
}
