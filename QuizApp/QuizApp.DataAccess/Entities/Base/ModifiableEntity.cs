﻿using System;

namespace QuizApp.DataAccess.Entities
{
    public class ModifiableEntity : BaseEntity
    {
        public DateTime? ModificationDate { get; set; }

        public int? ModifiedById { get; set; }

        public virtual UserEntity ModifiedBy { get; set; }
    }
}
