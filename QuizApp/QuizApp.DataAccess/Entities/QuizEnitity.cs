﻿using System.Collections.Generic;

namespace QuizApp.DataAccess.Entities
{
    public class QuizEnitity : ModifiableEntity
    {
        public string Name { get; set; }

        public int Difficulty { get; set; }

        public int TimeLimitInSeconds { get; set; }

        public int NumberOfQuestions { get; set; }

        public int CategoryId { get; set; }
        public virtual CategoryEntity Category { get; set; }

        public virtual IEnumerable<QuestionEntity> Questions { get; set; }

        public virtual IEnumerable<ResultEntity> Results { get; set; }
    }
}
