﻿using System;
using System.Collections.Generic;

namespace QuizApp.DataAccess.Entities
{
    public class UserEntity
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Role { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModificationDate { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActivated { get; set; }

        public string ActivationCode { get; set; }

        public virtual IEnumerable<QuizEnitity> CreatedQuizes { get; set; }

        public virtual IEnumerable<QuizEnitity> ModifiedQuizes { get; set; }

        public virtual IEnumerable<QuestionEntity> CreatedQuestions { get; set; }

        public virtual IEnumerable<QuestionEntity> ModifiedQuestions { get; set; }

        public virtual IEnumerable<ResultEntity> CreatedResults { get; set; }

        public virtual IEnumerable<CategoryEntity> CreatedCategories { get; set; }

        public virtual IEnumerable<CategoryEntity> ModifiedCategories { get; set; }
    }
}
