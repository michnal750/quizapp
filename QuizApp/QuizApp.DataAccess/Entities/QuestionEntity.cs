﻿namespace QuizApp.DataAccess.Entities
{
    public class QuestionEntity : ModifiableEntity
    {
        public string QuestionContent { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public int CorrectAnswerNumber { get; set; }

        public int QuizId { get; set; }
        public virtual QuizEnitity Quiz { get; set; }
    }
}
