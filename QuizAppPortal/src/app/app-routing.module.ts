import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { QuizListComponent } from './components/quiz-list/quiz-list.component';
import { RegisterComponent } from './components/register/register.component';
import { UserAccountComponent } from './components/user-account/user-account.component';
import { QuizComponent } from './components/quiz/quiz.component';
import { UserResultListComponent } from './components/user-result-list/user-result-list.component';
import { QuizAddModifyComponent } from './components/quiz-add-modify/quiz-add-modify.component';
import { UserQuizListComponent } from './components/user-quiz-list/user-quiz-list.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { UserListComponent } from './components/user-list/user-list.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{
		path: 'dashboard',
		component: DashboardComponent,
		children: [
			{
				path: 'quiz-list',
				component: QuizListComponent,
			},
			{
				path: 'user-result-list',
				component: UserResultListComponent,
			},
			{
				path: 'user-quiz-list',
				component: UserQuizListComponent,
			},
			{
				path: 'quiz/:id',
				component: QuizComponent,
			},
			{
				path: 'quiz-add',
				component: QuizAddModifyComponent,
			},
			{
				path: 'quiz-modify/:id',
				component: QuizAddModifyComponent,
			},
			{
				path: 'user-account',
				component: UserAccountComponent,
			},
			{
				path: 'user-account/:id',
				component: UserAccountComponent,
			},
			{
				path: 'category-list',
				component: CategoryListComponent,
			},
			{
				path: 'user-list',
				component: UserListComponent,
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
