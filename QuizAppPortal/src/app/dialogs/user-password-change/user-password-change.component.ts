import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserPasswordChange } from 'src/app/models/user-password-change';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-user-password-change',
	templateUrl: './user-password-change.component.html',
	styleUrls: ['./user-password-change.component.scss'],
})
export class UserPasswordChangeDialogComponent {
	form: FormGroup = new FormGroup({
		currentPassword: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		newPassword: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		newPasswordRepeated: new FormControl('', [
			Validators.required,
			Validators.maxLength(50),
		]),
	});
	isLoading = false;
	constructor(
		private readonly authService: AuthService,
		private readonly dialogRef: MatDialogRef<UserPasswordChangeDialogComponent>
	) {}

	get currentPassword() {
		return this.form.controls['currentPassword'];
	}
	get newPassword() {
		return this.form.controls['newPassword'];
	}

	get newPasswordRepeated() {
		return this.form.controls['newPasswordRepeated'];
	}

	onSubmit(): void {
		if (this.form.invalid || this.newPassword.value != this.newPasswordRepeated.value) {
			return;
		}

		this.isLoading = true;
		const passwordChange = {
			currentPassword: this.currentPassword.value,
			newPassword: this.newPassword.value,
		} as UserPasswordChange;

		this.authService.changePasswordByUser(passwordChange).subscribe(
			() => {
				this.dialogRef.close();
			},
			() => {
				this.isLoading = false;
			}
		);
	}
}
