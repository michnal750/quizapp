import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Result } from 'src/app/models/result';

@Component({
	selector: 'app-quiz-result',
	templateUrl: './quiz-result.component.html',
	styleUrls: ['./quiz-result.component.scss'],
})
export class QuizResultDialogComponent {
	constructor(
		public dialogRef: MatDialogRef<QuizResultDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: Result
	) {}

	tryAgain(): void {
		this.dialogRef.close(true);
	}

	getResult(): string {
		return `${this.data.numberOfCorrectAnswers} / ${this.data.numberOfAllQuestions}`;
	}

	getPercentageResult(): string {
		const percentage =
			(this.data.numberOfCorrectAnswers / this.data.numberOfAllQuestions) * 100;
		return `${percentage.toFixed(2)} %`;
	}
}
