import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from 'src/app/models/category';
import { CategoryAddModify } from 'src/app/models/category-add-modify';
import { CategoryService } from 'src/app/services/category.service';

@Component({
	selector: 'app-category-add-modify',
	templateUrl: './category-add-modify.component.html',
	styleUrls: ['./category-add-modify.component.scss'],
})
export class CategoryAddModifyDialogComponent implements OnInit {
	form: FormGroup;
	isLoading = false;
	constructor(
		private readonly categoryService: CategoryService,
		private readonly dialogRef: MatDialogRef<CategoryAddModifyDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: Category
	) {}

	ngOnInit(): void {
		this.setForm(this.data);
	}

	get categoryName() {
		return this.form.controls['categoryName'];
	}

	onSubmit(): void {
		if (this.form.invalid) {
			return;
		}

		this.isLoading = true;
		const categoryAddModify = {
			id: this.data?.id,
			name: this.categoryName.value,
		} as CategoryAddModify;

		if (this.data) {
			this.categoryService.update(categoryAddModify).subscribe(
				() => {
					this.dialogRef.close();
				},
				() => {
					this.isLoading = false;
				}
			);
		} else {
			this.categoryService.add(categoryAddModify).subscribe(
				() => {
					this.dialogRef.close();
				},
				() => {
					this.isLoading = false;
				}
			);
		}
	}

	private setForm(data: Category): void {
		this.form = new FormGroup({
			categoryName: new FormControl(data?.name, [
				Validators.required,
				Validators.maxLength(50),
			]),
		});
	}
}
