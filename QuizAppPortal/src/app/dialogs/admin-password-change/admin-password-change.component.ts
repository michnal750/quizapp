import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminPasswordChange } from 'src/app/models/admin-password-change';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-admin-password-change',
	templateUrl: './admin-password-change.component.html',
	styleUrls: ['./admin-password-change.component.scss'],
})
export class AdminPasswordChangeDialogComponent {
	form: FormGroup = new FormGroup({
		newPassword: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		newPasswordRepeated: new FormControl('', [
			Validators.required,
			Validators.maxLength(50),
		]),
	});
	isLoading = false;
	constructor(
		private readonly authService: AuthService,
		private readonly dialogRef: MatDialogRef<AdminPasswordChangeDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: number
	) {}

	get newPassword() {
		return this.form.controls['newPassword'];
	}

	get newPasswordRepeated() {
		return this.form.controls['newPasswordRepeated'];
	}

	onSubmit(): void {
		if (this.form.invalid || this.newPassword.value != this.newPasswordRepeated.value) {
			return;
		}

		this.isLoading = true;
		const passwordChange = {
			userId: this.data,
			newPassword: this.newPassword.value,
		} as AdminPasswordChange;

		this.authService.changePasswordByAdmin(passwordChange).subscribe(
			() => {
				this.dialogRef.close();
			},
			() => {
				this.isLoading = false;
			}
		);
	}
}
