import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { UserRole } from 'src/app/enums/user-role';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
	displayedColumns: string[] = [
		'id',
		'username',
		'firstName',
		'lastName',
		'email',
		'role',
		'createdDate',
		'actions',
	];
	dataSource: MatTableDataSource<User>;
	results: User[] = [];
	isLoading = false;
	userRole = UserRole;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	constructor(
		private readonly userService: UserService,
		private readonly router: Router
	) {}

	ngOnInit(): void {
		this.dataSource = new MatTableDataSource(this.results);
		this.fetchData();
	}

	fetchData(): void {
		this.isLoading = true;
		this.userService.getAll().subscribe((data: User[]) => {
			this.results = data;
			this.dataSource.data = this.results;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			this.isLoading = false;
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	modifyUser(selectedUser: User) {
		this.router.navigate(['/dashboard/user-account', selectedUser.id]);
	}

	deleteUser(selectedUser: User) {
		this.isLoading = true;
		this.userService.delete(selectedUser.id).subscribe(
			(data) => {
				this.fetchData();
				this.isLoading = false;
			},
			() => {
				this.isLoading = false;
			}
		);
	}
}
