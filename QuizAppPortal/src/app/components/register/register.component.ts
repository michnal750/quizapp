import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserRole } from 'src/app/enums/user-role';
import { UserAddModify } from 'src/app/models/user-add-modify';
import { UserService } from 'src/app/services/user.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
	form: FormGroup = new FormGroup({
		username: new FormControl('', [Validators.required, Validators.maxLength(20)]),
		password: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		passwordRepeat: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		firstName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		lastName: new FormControl('', [Validators.required, Validators.maxLength(50)]),
		email: new FormControl('', [
			Validators.required,
			Validators.email,
			Validators.maxLength(50),
		]),
	});
	isLoading = false;

	constructor(
		private readonly userService: UserService,
		private readonly router: Router,
		private readonly snackBar: MatSnackBar
	) {}

	ngOnInit(): void {}

	get username() {
		return this.form.controls['username'];
	}

	get password() {
		return this.form.controls['password'];
	}

	get passwordRepeat() {
		return this.form.controls['passwordRepeat'];
	}

	get firstName() {
		return this.form.controls['firstName'];
	}

	get lastName() {
		return this.form.controls['lastName'];
	}

	get email() {
		return this.form.controls['email'];
	}

	onSubmit() {
		if (this.form.invalid || this.password.value !== this.passwordRepeat.value) {
			return;
		}

		this.isLoading = true;
		const userAdd: UserAddModify = {
			id: null,
			username: this.username.value,
			password: this.password.value,
			firstName: this.firstName.value,
			lastName: this.lastName.value,
			email: this.email.value,
			role: UserRole.User,
		} as UserAddModify;

		this.userService.create(userAdd).subscribe(
			(data) => {
				this.backToLogin();
				this.snackBar.open(
					'Please click on the below link to activate your account:\n\n' +
						data.activationLink,
					'close'
				);
			},
			(error) => {
				this.isLoading = false;
				this.snackBar.open('register failed', 'close', {
					duration: 2000,
				});
			}
		);
	}

	backToLogin() {
		this.router.navigate(['/login']);
	}
}
