import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Result } from 'src/app/models/result';
import { ResultService } from 'src/app/services/result.service';

@Component({
	selector: 'app-user-result-list',
	templateUrl: './user-result-list.component.html',
	styleUrls: ['./user-result-list.component.scss'],
})
export class UserResultListComponent implements OnInit {
	displayedColumns: string[] = [
		'id',
		'quizId',
		'quizName',
		'result',
		'percentageResult',
		'createdDate',
	];
	dataSource: MatTableDataSource<Result>;
	results: Result[] = [];
	isLoading = false;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	constructor(private readonly resultService: ResultService) {}

	ngOnInit(): void {
		this.dataSource = new MatTableDataSource(this.results);
		this.fetchData();
	}

	fetchData(): void {
		this.isLoading = true;
		this.resultService.getAllUserResults().subscribe((data: Result[]) => {
			this.results = data;
			this.dataSource.data = this.results;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			this.isLoading = false;
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	getResult(row: Result): string {
		return `${row.numberOfCorrectAnswers} / ${row.numberOfAllQuestions}`;
	}

	getPercentageResult(row: Result): string {
		const percentage = (row.numberOfCorrectAnswers / row.numberOfAllQuestions) * 100;
		return `${percentage.toFixed(2)} %`;
	}
}
