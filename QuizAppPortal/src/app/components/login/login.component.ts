import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UserLogin } from 'src/app/models/user-login';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
	form: FormGroup = new FormGroup({
		username: new FormControl('', [Validators.required, Validators.maxLength(20)]),
		password: new FormControl('', [Validators.required, Validators.maxLength(50)]),
	});
	isLoading = false;
	numberOfLoginAttempt = 0;
	appBlocked = false;

	constructor(
		private readonly authService: AuthService,
		private readonly router: Router,
		private readonly snackBar: MatSnackBar
	) {}
	ngOnInit(): void {
		const blockedDate = parseInt(localStorage.getItem('blockedDate'));
		if (blockedDate) {
			const elapsedTime = Math.abs(blockedDate - Date.now());
			if (elapsedTime < 300000) {
				this.appBlocked = true;
				this.form.disable();
			}
		}
	}

	get username() {
		return this.form.controls['username'];
	}
	get password() {
		return this.form.controls['password'];
	}

	onSubmit() {
		if (this.form.invalid) {
			return;
		}

		this.isLoading = true;

		const userLogin: UserLogin = {
			username: this.username.value,
			password: this.password.value,
		} as UserLogin;

		this.authService.login(userLogin).subscribe(
			(data) => {
				this.numberOfLoginAttempt = 0;
				this.router.navigate(['/dashboard/quiz-list']);
			},
			(error) => {
				this.isLoading = false;
				this.numberOfLoginAttempt++;
				if (this.numberOfLoginAttempt === 5) {
					this.form.disable();
					this.appBlocked = true;
					localStorage.setItem('blockedDate', Date.now().toString());
					this.snackBar.open('5 login failed - app blocked', 'close');
				} else {
					this.snackBar.open('login failed', 'close', {
						duration: 2000,
					});
				}
			}
		);
	}

	register() {
		this.router.navigate(['/register']);
	}
}
