import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CategoryAddModifyDialogComponent } from 'src/app/dialogs/category-add-modify/category-add-modify.component';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
	selector: 'app-category-list',
	templateUrl: './category-list.component.html',
	styleUrls: ['./category-list.component.scss'],
})
export class CategoryListComponent implements OnInit {
	displayedColumns: string[] = ['id', 'name', 'createdDate', 'createdByData', 'actions'];
	dataSource: MatTableDataSource<Category>;
	categories: Category[] = [];
	isLoading = false;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	constructor(
		private readonly categoryService: CategoryService,
		public dialog: MatDialog
	) {}

	ngOnInit(): void {
		this.dataSource = new MatTableDataSource(this.categories);
		this.fetchData();
	}

	fetchData(): void {
		this.isLoading = true;
		this.categoryService.getAll().subscribe((data: Category[]) => {
			this.categories = data;
			this.dataSource.data = this.categories;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			this.isLoading = false;
		});
	}

	applyFilter(event: Event): void {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	addCategory(): void {
		const dialogRef = this.dialog.open(CategoryAddModifyDialogComponent);
		dialogRef.afterClosed().subscribe((confirmation: boolean) => {
			this.fetchData();
		});
	}

	updateCategory(category: Category): void {
		const dialogRef = this.dialog.open(CategoryAddModifyDialogComponent, {
			data: category,
		});
		dialogRef.afterClosed().subscribe((confirmation: boolean) => {
			this.fetchData();
		});
	}
}
