import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { QuestionAddModify } from 'src/app/models/question-add-modify';

@Component({
	selector: 'app-question-add-modify',
	templateUrl: './question-add-modify.component.html',
	styleUrls: ['./question-add-modify.component.scss'],
})
export class QuestionAddModifyComponent implements OnInit {
	@Input() question: QuestionAddModify;
	@Output() questionDataChangedEvent = new EventEmitter<QuestionAddModify>();
	@Output() deleteQuestionEvent = new EventEmitter<number>();

	form: FormGroup;
	correctAnswer: string;
	constructor() {}

	get questionContent() {
		return this.form.controls['questionContent'].value;
	}

	get answer1() {
		return this.form.controls['answer1'].value;
	}

	get answer2() {
		return this.form.controls['answer2'].value;
	}

	get answer3() {
		return this.form.controls['answer3'].value;
	}

	get answer4() {
		return this.form.controls['answer4'].value;
	}

	ngOnInit(): void {
		this.setForm(this.question);
		this.correctAnswer = this.question.correctAnswerNumber.toString();
		this.form.valueChanges.subscribe(() => {
			this.onDataChanged();
		});
	}

	radioChange($event: MatRadioChange): void {
		this.onDataChanged();
	}

	deleteQuestion(value: number): void {
		this.deleteQuestionEvent.emit(value);
	}

	private setForm(question: QuestionAddModify): void {
		this.form = new FormGroup({
			questionContent: new FormControl(question?.questionContent, [
				Validators.required,
				Validators.maxLength(200),
			]),
			answer1: new FormControl(question?.answer1, [
				Validators.required,
				Validators.maxLength(200),
			]),
			answer2: new FormControl(question?.answer2, [
				Validators.required,
				Validators.maxLength(200),
			]),
			answer3: new FormControl(question?.answer3, [
				Validators.required,
				Validators.maxLength(200),
			]),
			answer4: new FormControl(question?.answer4, [
				Validators.required,
				Validators.maxLength(200),
			]),
		});
	}

	private mapFormToModel(): void {
		this.question = {
			id: this.question.id,
			questionContent: this.questionContent,
			answer1: this.answer1,
			answer2: this.answer2,
			answer3: this.answer3,
			answer4: this.answer4,
			index: this.question.index,
			correctAnswerNumber: parseInt(this.correctAnswer),
		} as QuestionAddModify;
	}

	private onDataChanged(): void {
		this.mapFormToModel();
		this.questionDataChangedEvent.emit(this.question);
	}
}
