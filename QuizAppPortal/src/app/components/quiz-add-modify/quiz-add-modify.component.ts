import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Difficulty } from 'src/app/enums/difficulty';
import { Category } from 'src/app/models/category';
import { QuestionAddModify } from 'src/app/models/question-add-modify';
import { QuizAddModify } from 'src/app/models/quiz-add-modify';
import { QuizDetails } from 'src/app/models/quiz-details';
import { CategoryService } from 'src/app/services/category.service';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
	selector: 'app-quiz-add-modify',
	templateUrl: './quiz-add-modify.component.html',
	styleUrls: ['./quiz-add-modify.component.scss'],
})
export class QuizAddModifyComponent implements OnInit {
	isLoading = false;
	Difficulty = Difficulty;
	difficulties = [Difficulty.Easy, Difficulty.Medium, Difficulty.Hard];
	categories: Category[] = [];
	form: FormGroup;
	quizAddModify: QuizAddModify;

	constructor(
		private readonly quizService: QuizService,
		private readonly categoryService: CategoryService,
		private readonly router: Router,
		private readonly route: ActivatedRoute
	) {}

	get name() {
		return this.form.controls['name'].value;
	}

	get category() {
		return this.form.controls['category'].value;
	}

	get timeLimitInSeconds() {
		return this.form.controls['timeLimitInSeconds'].value;
	}

	get difficulty() {
		return this.form.controls['difficulty'].value;
	}

	ngOnInit(): void {
		this.setForm(null);
		const quizId: number = parseInt(this.route.snapshot.paramMap.get('id'));
		if (quizId) {
			this.quizService.getDetails(quizId).subscribe((details) => {
				this.mapDetailsToQuizAdd(details);
				this.setForm(this.quizAddModify);
				this.setCategoryForm();
			});
		} else {
			this.createEmptyQuiz();
			this.setForm(this.quizAddModify);
		}
		this.fetchCategories();
	}

	addQuestion(): void {
		const newQuestion = this.createNewQuestion(this.getNewQuestionIndex());
		this.quizAddModify.questions.push(newQuestion);
	}

	updateQuestion(updatedData: QuestionAddModify): void {
		const question: QuestionAddModify = this.quizAddModify.questions.find(
			(x) => x.index == updatedData.index
		);
		question.questionContent = updatedData.questionContent;
		question.answer1 = updatedData.answer1;
		question.answer2 = updatedData.answer2;
		question.answer3 = updatedData.answer3;
		question.answer4 = updatedData.answer4;
		question.correctAnswerNumber = updatedData.correctAnswerNumber;
	}

	deleteQuestion(index: number): void {
		this.quizAddModify.questions = this.quizAddModify.questions.filter(
			(x) => x.index != index
		);
	}

	saveQuiz(): void {
		if (this.form.invalid) {
			return;
		}
		this.isLoading = true;

		this.quizAddModify.name = this.name;
		this.quizAddModify.categoryId = this.category.id;
		this.quizAddModify.timeLimitInSeconds = this.timeLimitInSeconds;
		this.quizAddModify.difficulty = this.difficulty;
		if (this.quizAddModify.id) {
			this.quizService.update(this.quizAddModify).subscribe(
				(data: QuizDetails) => {
					console.log(data);
					this.isLoading = false;
					this.router.navigate(['/dashboard/quiz', data.id]);
				},
				() => {
					this.isLoading = false;
				}
			);
		} else {
			this.quizService.add(this.quizAddModify).subscribe(
				(data: QuizDetails) => {
					console.log(data);
					this.isLoading = false;
					this.router.navigate(['/dashboard/quiz', data.id]);
				},
				() => {
					this.isLoading = false;
				}
			);
		}
	}

	private setForm(quiz: QuizAddModify): void {
		this.form = new FormGroup({
			name: new FormControl(quiz?.name, [Validators.required, Validators.maxLength(50)]),
			category: new FormControl(null, Validators.required),
			timeLimitInSeconds: new FormControl(quiz?.timeLimitInSeconds, [
				Validators.required,
				Validators.min(1),
			]),
			difficulty: new FormControl(quiz?.difficulty, Validators.required),
		});
	}

	private mapDetailsToQuizAdd(details: QuizDetails): void {
		this.quizAddModify = {
			id: details.id,
			name: details.name,
			categoryId: details.categoryId,
			timeLimitInSeconds: details.timeLimitInSeconds,
			difficulty: details.difficulty,
		} as QuizAddModify;
		let index = 0;
		this.quizAddModify.questions = details.questions.map(
			(x) =>
				({
					id: x.id,
					questionContent: x.questionContent,
					answer1: x.answer1,
					answer2: x.answer2,
					answer3: x.answer3,
					answer4: x.answer4,
					correctAnswerNumber: x.correctAnswerNumber,
					index: index++,
				} as QuestionAddModify)
		);
	}

	private createEmptyQuiz(): void {
		this.quizAddModify = new QuizAddModify();
		this.quizAddModify.questions = [];

		const initialQuestion = this.createNewQuestion(0);
		this.quizAddModify.questions.push(initialQuestion);
	}

	private createNewQuestion(index: number): QuestionAddModify {
		return {
			questionContent: 'Question',
			answer1: 'Answer 1',
			answer2: 'Answer 2',
			answer3: 'Answer 3',
			answer4: 'Answer 4',
			correctAnswerNumber: 1,
			index: index,
		} as QuestionAddModify;
	}

	private getNewQuestionIndex(): number {
		const indexArray = this.quizAddModify.questions.map((x) => x.index);
		const maxIndex = Math.max(...indexArray);
		return maxIndex + 1;
	}

	private fetchCategories() {
		this.categoryService.getAll().subscribe((data) => {
			this.categories = data;
			this.setCategoryForm();
		});
	}

	private setCategoryForm() {
		if (this.quizAddModify.id) {
			const category: Category = this.categories.find(
				(x) => x.id === this.quizAddModify.categoryId
			);
			this.form.get('category').setValue(category);
		}
	}
}
