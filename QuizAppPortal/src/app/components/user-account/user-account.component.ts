import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { AdminPasswordChangeDialogComponent } from 'src/app/dialogs/admin-password-change/admin-password-change.component';
import { UserPasswordChangeDialogComponent } from 'src/app/dialogs/user-password-change/user-password-change.component';
import { User } from 'src/app/models/user';
import { UserAddModify } from 'src/app/models/user-add-modify';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
	selector: 'app-user-account',
	templateUrl: './user-account.component.html',
	styleUrls: ['./user-account.component.scss'],
})
export class UserAccountComponent implements OnInit {
	private user: User;
	private userId: number;
	adminMode: boolean;
	form: FormGroup;
	isLoading: boolean = false;

	constructor(
		private readonly userService: UserService,
		private readonly authService: AuthService,
		public dialog: MatDialog,
		private readonly route: ActivatedRoute
	) {}

	get firstName() {
		return this.form.controls['firstName'];
	}

	get lastName() {
		return this.form.controls['lastName'];
	}

	get email() {
		return this.form.controls['email'];
	}

	ngOnInit(): void {
		this.userId = parseInt(this.route.snapshot.paramMap.get('id'));
		if (this.userId) {
			this.adminMode = true;
		} else {
			this.adminMode = false;
			this.userId = this.authService.getUser()?.id;
		}

		this.setForm(this.user);
		this.fetchData();
	}

	fetchData(): void {
		this.isLoading = true;
		this.userService.get(this.userId).subscribe((data) => {
			this.user = data;
			this.setForm(this.user);
			this.isLoading = false;
		});
	}

	update() {
		if (this.form.invalid) {
			return;
		}

		this.isLoading = true;
		const userUpdate: UserAddModify = {
			id: this.user.id,
			firstName: this.firstName.value,
			lastName: this.lastName.value,
			email: this.email.value,
		} as UserAddModify;

		this.userService.update(userUpdate).subscribe((data) => {
			this.user = data;
			this.setForm(this.user);
			this.isLoading = false;
		});
	}

	private setForm(user: User): void {
		this.form = new FormGroup({
			username: new FormControl(user?.username, [
				Validators.required,
				Validators.maxLength(20),
			]),
			firstName: new FormControl(user?.firstName, [
				Validators.required,
				Validators.maxLength(50),
			]),
			lastName: new FormControl(user?.lastName, [
				Validators.required,
				Validators.maxLength(50),
			]),
			email: new FormControl(user?.email, [
				Validators.required,
				Validators.email,
				Validators.maxLength(50),
			]),
			createdDate: new FormControl(
				formatDate(user?.createdDate ?? null, 'yyyy-MM-dd', 'en')
			),
		});
	}

	changePassword(): void {
		if (this.adminMode) {
			this.dialog.open(AdminPasswordChangeDialogComponent, { data: this.userId });
		} else {
			this.dialog.open(UserPasswordChangeDialogComponent);
		}
	}
}
