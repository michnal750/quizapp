import { Component } from '@angular/core';
import { UserRole } from 'src/app/enums/user-role';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
	constructor(private readonly authService: AuthService) {}

	getUserFormattedData(): string {
		const user = this.authService.getUser();
		return `${user.firstName} ${user.lastName}`;
	}

	isAdmin(): boolean {
		return this.authService.hasRole(UserRole.Admin);
	}

	logout() {
		this.authService.logout();
	}
}
