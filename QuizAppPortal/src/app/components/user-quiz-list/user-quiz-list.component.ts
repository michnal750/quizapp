import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Difficulty } from 'src/app/enums/difficulty';
import { Quiz } from 'src/app/models/quiz';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
	selector: 'app-user-quiz-list',
	templateUrl: './user-quiz-list.component.html',
	styleUrls: ['./user-quiz-list.component.scss'],
})
export class UserQuizListComponent implements OnInit {
	displayedColumns: string[] = [
		'id',
		'name',
		'category',
		'difficulty',
		'createdDate',
		'actions',
	];
	dataSource: MatTableDataSource<Quiz>;
	quizes: Quiz[] = [];
	isLoading = false;
	difficulty = Difficulty;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	constructor(
		private readonly quizService: QuizService,
		private readonly router: Router
	) {}

	ngOnInit(): void {
		this.dataSource = new MatTableDataSource(this.quizes);
		this.fetchData();
	}

	fetchData(): void {
		this.isLoading = true;
		this.quizService.getAllUserQuizes().subscribe((data: Quiz[]) => {
			this.quizes = data;
			this.dataSource.data = this.quizes;
			this.dataSource.sort = this.sort;
			this.dataSource.paginator = this.paginator;
			this.isLoading = false;
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();

		if (this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	selectQuiz(selectedQuiz: Quiz) {
		this.router.navigate(['/dashboard/quiz', selectedQuiz.id]);
	}

	modifyQuiz(selectedQuiz: Quiz) {
		this.router.navigate(['/dashboard/quiz-modify', selectedQuiz.id]);
	}

	deleteQuiz(selectedQuiz: Quiz) {
		this.quizService.delete(selectedQuiz.id).subscribe(() => {
			this.fetchData();
		});
	}
}
