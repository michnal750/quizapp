import { Component, Input } from '@angular/core';
import { Difficulty } from 'src/app/enums/difficulty';
import { QuizDetails } from 'src/app/models/quiz-details';

@Component({
	selector: 'app-quiz-details',
	templateUrl: './quiz-details.component.html',
	styleUrls: ['./quiz-details.component.scss'],
})
export class QuizDetailsComponent {
	@Input() quizDetails: QuizDetails;
	difficulty = Difficulty;
	constructor() {}
}
