import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute } from '@angular/router';
import { CountdownEvent } from 'ngx-countdown';
import { QuizResultDialogComponent } from 'src/app/dialogs/quiz-result/quiz-result.component';
import { CompletedQuiz } from 'src/app/models/completed-quiz';
import { CompletedQuizAnswer } from 'src/app/models/completed-quiz-answer';
import { Question } from 'src/app/models/question';
import { QuizDetails } from 'src/app/models/quiz-details';
import { AuthService } from 'src/app/services/auth.service';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
	selector: 'app-quiz',
	templateUrl: './quiz.component.html',
	styleUrls: ['./quiz.component.scss'],
})
export class QuizComponent implements OnInit {
	quizDetails: QuizDetails;
	hasStarted: boolean = false;

	selectedQuestion: Question;
	selectedQuestionIndex: number;
	numberOfQuestions: number;

	selectedAnswer: string;
	completedQuiz: CompletedQuiz;

	constructor(
		private readonly quizService: QuizService,
		private readonly route: ActivatedRoute,
		private readonly authService: AuthService,
		private readonly dialog: MatDialog
	) {}

	ngOnInit(): void {
		const quizId: number = parseInt(this.route.snapshot.paramMap.get('id'));
		this.quizService.getDetails(quizId).subscribe((data) => {
			this.quizDetails = data;
		});
	}

	startQuiz(): void {
		this.numberOfQuestions = this.quizDetails.questions.length;
		this.selectedQuestionIndex = 0;
		this.selectedQuestion = this.quizDetails.questions[this.selectedQuestionIndex];
		this.selectedAnswer = null;
		this.createDraftSolvedQuiz();
		this.hasStarted = true;
	}

	stopQuiz(): void {
		this.hasStarted = false;
	}

	previousQuestion(): void {
		this.selectedQuestionIndex--;
		this.selectedQuestion = this.quizDetails.questions[this.selectedQuestionIndex];
		this.selectedAnswer = this.getUserAnswer(this.selectedQuestionIndex);
	}

	nextQuestion(): void {
		if (this.selectedQuestionIndex + 1 < this.numberOfQuestions) {
			this.selectedQuestionIndex++;
			this.selectedQuestion = this.quizDetails.questions[this.selectedQuestionIndex];
			this.selectedAnswer = this.getUserAnswer(this.selectedQuestionIndex);
		} else {
			this.checkQuiz();
		}
	}

	radioChange($event: MatRadioChange) {
		this.mapUserAnswerToSolvedQuiz(
			this.selectedQuestionIndex,
			parseInt(this.selectedAnswer)
		);
	}

	handleEvent(event: CountdownEvent) {
		if (event.left === 0) {
			this.checkQuiz();
		}
	}

	private createDraftSolvedQuiz(): void {
		const selectedAnswers: CompletedQuizAnswer[] = [];
		this.quizDetails.questions.forEach((question) => {
			selectedAnswers.push({
				questionId: question.id,
				selectedAnswer: null,
			} as CompletedQuizAnswer);
		});
		this.completedQuiz = {
			userId: this.authService.getUser().id,
			quizId: this.quizDetails.id,
			selectedAnswers: selectedAnswers,
		} as CompletedQuiz;
	}

	private mapUserAnswerToSolvedQuiz(questionIndex: number, selectedAnswer: number): void {
		const actualQuestionId: number = this.quizDetails.questions[questionIndex].id;
		this.completedQuiz.selectedAnswers.find(
			(x) => x.questionId === actualQuestionId
		).selectedAnswer = selectedAnswer;
	}

	private getUserAnswer(questionIndex: number): string {
		const questionId: number = this.quizDetails.questions[questionIndex].id;
		return this.completedQuiz.selectedAnswers
			.find((x) => x.questionId == questionId)
			.selectedAnswer?.toString();
	}

	private checkQuiz(): void {
		this.stopQuiz();
		this.quizService.checkCompletedQuiz(this.completedQuiz).subscribe((data) => {
			const dialogRef = this.dialog.open(QuizResultDialogComponent, {
				height: '400px',
				width: '550px',
				data: data,
			});
			dialogRef.afterClosed().subscribe((confirmation: boolean) => {
				if (confirmation) {
					this.startQuiz();
				}
			});
		});
	}
}
