export class Result {
	id: number;
	quizId: number;
	quizName: string;
	userId: number;
	numberOfCorrectAnswers: number;
	numberOfAllQuestions: number;
	createdDate: Date;
}
