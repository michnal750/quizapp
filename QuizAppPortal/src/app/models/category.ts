export class Category {
	id: number;
	name: string;
	createdDate: Date;
	createdById: number;
	createdByData: string;
}
