import { UserRole } from '../enums/user-role';

export class User {
	id: number;
	username: string;
	password: string;
	firstName: string;
	lastName: string;
	email: string;
	role: UserRole;
	createdDate: Date;
	activationLink: string;
}
