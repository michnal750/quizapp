import { UserRole } from '../enums/user-role';

export class UserAddModify {
	id: number;
	username: string;
	password: string;
	email: string;
	firstName: string;
	lastName: string;
	role: UserRole;
}
