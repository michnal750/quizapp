export class Quiz {
	id: number;
	name: string;
	categoryId: number;
	categoryName: string;
	difficulty: number;
	createdDate: Date;
	createdById: number;
	createdByData: string;
}
