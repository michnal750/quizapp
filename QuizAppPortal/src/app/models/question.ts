export class Question {
	id: number;
	questionContent: string;
	answer1: string;
	answer2: string;
	answer3: string;
	answer4: string;
	correctAnswerNumber: number;
}
