import { QuestionAddModify } from './question-add-modify';

export class QuizAddModify {
	id: number;
	name: string;
	categoryId: number;
	difficulty: number;
	timeLimitInSeconds: number;
	questions: QuestionAddModify[];
}
