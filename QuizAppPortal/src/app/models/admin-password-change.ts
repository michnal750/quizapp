export class AdminPasswordChange {
	userId: number;
	newPassword: string;
}
