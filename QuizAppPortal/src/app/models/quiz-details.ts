import { Question } from './question';

export class QuizDetails {
	id: number;
	name: string;
	categoryId: number;
	categoryName: string;
	difficulty: number;
	timeLimitInSeconds: number;
	numberOfQuestions: number;
	createdDate: Date;
	createdById: number;
	createdByData: string;
	modificationDate: Date;
	modifiedById: number;
	modifiedByData: string;
	questions: Question[];
}
