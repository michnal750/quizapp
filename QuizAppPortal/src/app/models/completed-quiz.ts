import { CompletedQuizAnswer } from './completed-quiz-answer';

export class CompletedQuiz {
	quizId: number;
	userId: number;
	selectedAnswers: CompletedQuizAnswer[];
}
