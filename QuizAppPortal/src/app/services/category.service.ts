import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../models/category';
import { CategoryAddModify } from '../models/category-add-modify';

@Injectable({
	providedIn: 'root',
})
export class CategoryService {
	constructor(private readonly http: HttpClient) {}

	getAll(): Observable<Category[]> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<Category[]>(`${environment.apiUrl}/category`, options);
	}

	add(categoryAdd: CategoryAddModify): Observable<Category> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.post<Category>(
			`${environment.apiUrl}/category`,
			categoryAdd,
			options
		);
	}

	update(categoryModify: CategoryAddModify): Observable<Category> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.put<Category>(
			`${environment.apiUrl}/category`,
			categoryModify,
			options
		);
	}
}
