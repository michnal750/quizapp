import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CompletedQuiz } from '../models/completed-quiz';
import { Quiz } from '../models/quiz';
import { QuizAddModify } from '../models/quiz-add-modify';
import { QuizDetails } from '../models/quiz-details';
import { Result } from '../models/result';

@Injectable({
	providedIn: 'root',
})
export class QuizService {
	constructor(private readonly http: HttpClient) {}

	getAll(): Observable<Quiz[]> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<Quiz[]>(`${environment.apiUrl}/quiz`, options);
	}

	getAllUserQuizes(): Observable<Quiz[]> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<Quiz[]>(`${environment.apiUrl}/quiz/user`, options);
	}

	getDetails(quizId: number): Observable<QuizDetails> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<QuizDetails>(`${environment.apiUrl}/quiz/${quizId}`, options);
	}

	add(quizAdd: QuizAddModify): Observable<QuizDetails> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.post<QuizDetails>(`${environment.apiUrl}/quiz`, quizAdd, options);
	}

	update(quizUpdate: QuizAddModify): Observable<QuizDetails> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.put<QuizDetails>(`${environment.apiUrl}/quiz`, quizUpdate, options);
	}

	delete(quizId: number): Observable<QuizDetails> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.delete<QuizDetails>(`${environment.apiUrl}/quiz/${quizId}`, options);
	}

	checkCompletedQuiz(completedQuiz: CompletedQuiz): Observable<Result> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.post<Result>(
			`${environment.apiUrl}/quiz/check`,
			completedQuiz,
			options
		);
	}
}
