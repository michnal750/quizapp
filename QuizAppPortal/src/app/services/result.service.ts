import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Result } from '../models/result';

@Injectable({
	providedIn: 'root',
})
export class ResultService {
	constructor(private readonly http: HttpClient) {}

	getAllUserResults(): Observable<Result[]> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<Result[]>(`${environment.apiUrl}/result`, options);
	}
}
