import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';
import { UserAddModify } from '../models/user-add-modify';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	constructor(private readonly http: HttpClient) {}

	get(userId: number): Observable<User> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<User>(`${environment.apiUrl}/user/${userId}`, options);
	}

	getAll(): Observable<User[]> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.get<User[]>(`${environment.apiUrl}/user`, options);
	}

	create(userAdd: UserAddModify): Observable<User> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.post<User>(`${environment.apiUrl}/user`, userAdd, options);
	}

	update(userModify: UserAddModify): Observable<User> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.put<User>(`${environment.apiUrl}/user`, userModify, options);
	}

	delete(userId: number): Observable<User> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};

		return this.http.delete<User>(`${environment.apiUrl}/user/${userId}`, options);
	}
}
