import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserRole } from '../enums/user-role';
import { AdminPasswordChange } from '../models/admin-password-change';
import { User } from '../models/user';
import { UserLogin } from '../models/user-login';
import { UserPasswordChange } from '../models/user-password-change';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	private loggedUser$ = new BehaviorSubject<User>(null);

	constructor(private readonly http: HttpClient, private readonly router: Router) {
		const loggedUserSerialized = localStorage.getItem('loggedUser');
		if (loggedUserSerialized) {
			const loggedUser = JSON.parse(loggedUserSerialized);
			this.loggedUser$.next(loggedUser.user);
		}
	}

	login(userLogin: UserLogin): Observable<any> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};
		return this.http.post(`${environment.apiUrl}/auth/login`, userLogin, options).pipe(
			map((response: any) => {
				console.log(response);
				this.loggedUser$.next(response.user);
				localStorage.setItem('loggedUser', JSON.stringify(response));
				return response;
			})
		);
	}

	changePasswordByUser(passwordChange: UserPasswordChange): Observable<any> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};
		return this.http.patch(
			`${environment.apiUrl}/auth/password/by-user`,
			passwordChange,
			options
		);
	}

	changePasswordByAdmin(passwordChange: AdminPasswordChange): Observable<any> {
		const options = {
			headers: new HttpHeaders().set('Content-Type', 'application/json'),
		};
		return this.http.patch(
			`${environment.apiUrl}/auth/password/by-admin`,
			passwordChange,
			options
		);
	}

	logout(): void {
		localStorage.removeItem('loggedUser');
		this.loggedUser$.next(null);
		this.router.navigate(['/login']);
	}

	isLoggedIn(): boolean {
		return !!this.loggedUser$.value;
	}

	getUser(): User {
		return this.loggedUser$?.value;
	}

	getToken(): string {
		const loggedUserSerialized = localStorage.getItem('loggedUser');
		const loggedUser = JSON.parse(loggedUserSerialized);
		return loggedUser?.token;
	}

	hasRole(role: UserRole): boolean {
		return this.loggedUser$.value?.role === role;
	}
}
