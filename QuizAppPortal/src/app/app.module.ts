import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CountdownModule } from 'ngx-countdown';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { QuizListComponent } from './components/quiz-list/quiz-list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { UserAccountComponent } from './components/user-account/user-account.component';
import { UserPasswordChangeDialogComponent } from './dialogs/user-password-change/user-password-change.component';
import { QuizComponent } from './components/quiz/quiz.component';
import { QuizDetailsComponent } from './components/quiz/quiz-details/quiz-details.component';
import { QuizResultDialogComponent } from './dialogs/quiz-result/quiz-result.component';
import { UserResultListComponent } from './components/user-result-list/user-result-list.component';
import { QuizAddModifyComponent } from './components/quiz-add-modify/quiz-add-modify.component';
import { QuestionAddModifyComponent } from './components/quiz-add-modify/question-add-modify/question-add-modify.component';
import { UserQuizListComponent } from './components/user-quiz-list/user-quiz-list.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { CategoryAddModifyDialogComponent } from './dialogs/category-add-modify/category-add-modify.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AdminPasswordChangeDialogComponent } from './dialogs/admin-password-change/admin-password-change.component';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		RegisterComponent,
		QuizListComponent,
		DashboardComponent,
		UserAccountComponent,
		UserPasswordChangeDialogComponent,
		AdminPasswordChangeDialogComponent,
		QuizComponent,
		QuizDetailsComponent,
		QuizResultDialogComponent,
		UserResultListComponent,
		QuizAddModifyComponent,
		QuestionAddModifyComponent,
		UserQuizListComponent,
		CategoryListComponent,
		CategoryAddModifyDialogComponent,
		UserListComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		ReactiveFormsModule,
		CommonModule,
		FormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatToolbarModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatButtonModule,
		MatInputModule,
		MatCardModule,
		MatProgressSpinnerModule,
		MatTableModule,
		MatSortModule,
		MatPaginatorModule,
		MatDialogModule,
		MatRadioModule,
		MatSelectModule,
		CountdownModule,
		MatSnackBarModule,
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: TokenInterceptor,
			multi: true,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
